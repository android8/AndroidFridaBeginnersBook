package com.yemao.demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.roysue.demo.R;

public class MainActivity extends AppCompatActivity {

    class 非重载测试类 {
        public int 中文方法(int x , int y) {
            Log.d("yemao.mul" , String.valueOf(x*y));
            return x * y;
        }
    }

    private String total = "hello";
    private 非重载测试类 非重载测试类实例 = new 非重载测试类();

    private void 测试() {
        Log.d("yemao", "测试=================");
//        try {
//            Thread.sleep(3*1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        非重载测试类实例.中文方法(50, 30);
        fun(50,30);
        Log.d("yemao.string" , fun("LoWeRcAsE Me!!!!!!!!!"));

        // 构造函数测试
        // String s = new String("Test...");
        非重载测试类 非重载测试类实例 = new 非重载测试类();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button=findViewById(R.id.button_test);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                测试();
            }
        });
    }
    void fun(int x , int y ){
        Log.d("yemao.sum" , String.valueOf(x+y));
    }
    String fun(String x){
        return x.toLowerCase();
    }

    void secret(){
        total += " secretFunc";
        Log.d("yemao.secret" , "this is secret func");
    }
    static void staticSecret(){
        Log.d("yemao.secret" , "this is static secret func");
    }
}