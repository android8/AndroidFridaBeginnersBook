package com.roysue.httpurlconnectiondemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {

    public void ezReq(String urlStr) {
        try {
            URL url = new URL(urlStr);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("token","demo");
            connection.setConnectTimeout(8000);
            connection.setReadTimeout(8000);
            connection.connect(); // 开始连接
            InputStream in = connection.getInputStream();
            //if(in.available() > 0){
                // 每次写入1024字节
                int bufferSize = 1024;
                byte[] buffer = new byte[bufferSize];
                StringBuffer sb = new StringBuffer();
                while ((in.read(buffer)) != -1) {
                    sb.append(new String(buffer));
                }
                Log.d("demo", sb.toString());
                connection.disconnect();
            // }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.buttonReq);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //请求详情
                        TextView viewById = findViewById(R.id.textInputEditText);
                        String url = viewById.getText().toString();
                        Log.d("url = ", url);
                        ezReq(url);
                    }
                })).start();
            }
        });
    }
}